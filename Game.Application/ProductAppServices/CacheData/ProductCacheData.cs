﻿using Abp.AutoMapper;
using Game.Core.Entity.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.ProductAppServices.CacheData
{
    [AutoMap(typeof(Product))]
    public class ProductCacheData
    {
        /// <summary>
        /// 产品类别
        /// </summary>
        public int ClassId { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 市场价
        /// </summary>
        public decimal MarketPrice { get; set; }
        /// <summary>
        /// 商场价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 是否发布
        /// </summary>
        public bool IsPublish { get; set; }
        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PublishTime { get; set; }
    }
}
