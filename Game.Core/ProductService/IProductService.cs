﻿using Abp.Domain.Services;
using Game.Core.Entity.Products;
using Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.ProductService
{
    public interface IProductService : IEntityDomainService<Product, long>
    {
    }
}
