﻿using Abp.Domain.Entities.Caching;
using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using Game.Core.Entity.Products;
using Game.Core.Entity.Users;
using Game.ProductAppServices.CacheData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.ProductAppServices.CacheManager
{
    public class ProductCache : EntityCache<Product, ProductCacheData, long>, IProductCache
    {
        public ProductCache(ICacheManager cacheManager, IRepository<Product, long> repository, string cacheName = "Product")
            :base(cacheManager,repository,cacheName)
        {

        }
    }
}
