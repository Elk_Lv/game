﻿using Game.ProductAppServices.CacheManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.ProductAppServices
{
    public class ProductAppService : GameAppServiceBase, IProductAppService
    {
        private readonly IProductCache _productCache;

        public ProductAppService(IProductCache productCache)
        {
            _productCache = productCache;
        }
    }
}
