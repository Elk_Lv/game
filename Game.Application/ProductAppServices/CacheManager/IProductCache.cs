﻿using Abp.Dependency;
using Abp.Domain.Entities.Caching;
using Game.ProductAppServices.CacheData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.ProductAppServices.CacheManager
{
    public interface IProductCache : IEntityCache<ProductCacheData, long>, ITransientDependency
    {
    }
}
