﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Microsoft.AspNet.Identity;

namespace Game.Core.Entity.Users
{

    public class User : FullAuditedEntity<long>, IMayHaveTenant, IUser<long>, IFullAudited<User>
    {
        public const string PasswordHasher = "123456";
        public const string AdminUserName = "Elk_Lv";

        public User()
        {
            PassWord = new PasswordHasher().HashPassword(PasswordHasher);
        }

        public string UserName { get; set; }

        public string PassWord { get; set; }

        public User CreatorUser { get; set; }

        public User LastModifierUser { get; set; }

        public User DeleterUser { get; set; }

        public int? TenantId { get; set; }

        public static User CreateAdminUser(string password, int tenantId = 1)
        {
            return new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                PassWord = new PasswordHasher().HashPassword(password)
            };
        }
    }
}
